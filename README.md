# “Getting to Philosophy”
An app that takes a Wikipedia URL as input, and display
the paths taken from clicking the first link of each page until you get to the Philosophy article.

## Result
* The front-end is deployed here: [https://tinnvo.bitbucket.io/](https://tinnvo.bitbucket.io/)

* The back-end is deployed here: [https://glacial-island-75519.herokuapp.com](https://glacial-island-75519.herokuapp.com)

* Max hops, current the maximum number of hops are 250, if it reaches that number, then it will stop (usually it can reach to the Philosophy article before that number);

* If bad URL is provided, then it will display the TOTAL HOPS: -1 on the UI.

**NOTE:** *I used MLAB as the database for this app to make it simple. Here I will provide login creds so that you can login and see the database's details. I will remove it after we're done with it.*

**NOTE:** *Also, I put the creds in the Spring property to connect to the database directly. Of course, in actual production, those creds will be the environment variables.*

Creds:
```
MLAB: https://mlab.com/login/
User: bentochallenge
Pass: bento123
```
## How the back-end work?
The back end for this one is pretty simple, it can do two things:

1. It can take the URL, process, and display the paths taken from the URL to the Philosophy article.
  ```
    https://glacial-island-75519.herokuapp.com/philosophy?providedUrl=<providedUrl>
  ```
  ```
    <---------------------HOST---------------><--route---><---param--><--the-url-->  
  ```
  The request link can be broken into: the host, the route, the param, and the URL
  For example:
  I want to find the paths from the below link to the Philosophy article:

  ```
  https://en.wikipedia.org/wiki/Pho
  ```
  I can do:
  ```
  https://glacial-island-75519.herokuapp.com/philosophy?providedUrl=https://en.wikipedia.org/wiki/Pho
  ```

  The response will contain the number of hops, the paths (with unique id), and the target/provided URL. It will have this format:

  ```
  {
    "hops": <number of hops>,
    "urls": [
      {
        "id": "<url id>",
        "url": "<url>"
      },
      ...
      ...
      ...
    ],
    "providedUrl": "<provided URL>"
  }
  ```

2. It can remove all of the records in the (MLAB) database by invoking this link:
  ```
  https://glacial-island-75519.herokuapp.com/delete
  ```

## Prerequisites (if want to run locally)
Java 8, Maven 3.5.4, yarn or npm


### Installing

1. Clone the repository:
  ```
    git clone https://tinnvo@bitbucket.org/tinnvo/bento.git
  ```
2. Start the server. Change directory to the server and run the server:
  ```
    $ cd bento/server
  ```
  ```
    $ ./mvnw spring-boot:run
  ```

  The server is live at: http://localhost:8080

3. Testing:

  * Using your browser. Simply open your browser and type in this:
  ```
  http://localhost:8080/philosophy?providedUrl=https://en.wikipedia.org/wiki/Pho
  ```

  * Using the client. Open another terminal, and cd to the bento/client directory and start the client server
    ```
    $ cd bento/client
    ```
    ```
    $ yarn start
    ```

  The client is live at: http://localhost:3000/

  This is similar to the one that is deployed on https://tinnvo.bitbucket.io/

## Challenges
  The hard part is to parse the HTML document to get to the first link, and I have to take sure that I do not use the same URL over a over again.

## Improvements?
  There are some things that can be improved:

* Allow the user to set the max hops. This is just in case the user provides an URL that take more than 250 hops, allowing them to set it so that they can see the desire results.

* Validation for bad URLs.

* API should be improved so that it can be updated the existing record, get only one record, get all records.

* The user can have an option to delete the records from the UI.

* There should be a section for "recent links" that shows the user the links that were used recently.

## Built With

* [Spring](http://spring.io/) - The framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [Jsoup](https://jsoup.org/) - Java library for working with real-world HTM
* [Mongodb](https://www.mongodb.com/) - Database
* [ReactJS](https://reactjs.org/) - JavaScript library for building user interfaces.

## Authors

* **Tin Vo** - *Initial work* - [Tin Vo](https://tinnvo.github.io/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
