import React, { Component } from 'react';
import Url from './Url';

class UrlList extends React.Component {

  render() {

    const urlComponent = this.props.philResults.urls.map((url) => (
      <Url
        key={"url-" + url.id}
        id={url.id}
        url={url.url}
      />
    ));
    return (
      <div className='container'>
        <p className="lead">
          TOTAL HOPS: {this.props.philResults.hops}
        </p>
        <ol> {urlComponent} </ol>
      </div>
    );
  }
}

export default UrlList;
