import React, { Component } from 'react';

class Url extends React.Component {
  render() {
    return (
        <li className="text-left">
          <a target="_blank" href={this.props.url}>{this.props.url}</a>
        </li>
    );
  }
}

export default Url;
