import React, { Component } from 'react';
import pacman from '../assets/Pacman.gif'

class Spinner extends React.Component {
  render() {
    return (
      <img src={pacman} className='footericon' alt='pacman'/>
    );
  }
}

export default Spinner;
