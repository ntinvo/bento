import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios';
import UrlList from './components/UrlList';
import Spinner from './components/Spinner';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      loading: false,
      data: {},
      returned: false
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    event.preventDefault();
    // https://glacial-island-75519.herokuapp.com/
    const url = "https://glacial-island-75519.herokuapp.com/philosophy?providedUrl=" + this.state.value;
    this.setState({loading: true, returned: false});
    var self = this;
    axios.post(url).then(function(res) {
      self.setState({loading: false, data: res.data, returned: true})
    }).catch(function (error) {
      // error goes here
      // console.log(error);
    })
  }

  render() {
    var data = this.state.data;
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">“Getting to Philosophy”</h1>
        </header>
        <br />
        <form onSubmit={this.handleSubmit} className="container">
          <div className="form-group">
            <label>URL</label>
            <input required className="form-control" id="url1" placeholder="Enter a wikipedia link" value={this.state.value} onChange={this.handleChange}/>
          </div>
          <button type="submit" value="Submit" className="btn btn-primary">Submit</button>
        </form>

        <br />
        { this.state.loading && <Spinner/> }
        { this.state.returned && <UrlList philResults={this.state.data}/> }
      </div>
    );
  }
}

export default App;
