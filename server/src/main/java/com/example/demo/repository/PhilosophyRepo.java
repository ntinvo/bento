package com.example.demo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.demo.model.PhilosophyResult;

public interface PhilosophyRepo extends MongoRepository<PhilosophyResult, String>{
	
}
