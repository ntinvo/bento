package com.example.demo.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.PhilosophyResult;
import com.example.demo.model.Url;
import com.example.demo.repository.PhilosophyRepo;

@Service
public class UrlService {
	@Autowired
	private PhilosophyRepo philosophyRepo;
	
	
	/*
	 * save the result to the database service 
	 */
	public PhilosophyResult create(ArrayList<String> urls, String providedUrl, int hops) {
		ArrayList<Url> tmpUrls = new ArrayList<>();
		for(String url: urls) {
			Url currentUrl = new Url(url);
			tmpUrls.add(currentUrl);
		}
		return philosophyRepo.save(new PhilosophyResult(tmpUrls, providedUrl, hops));
	}

	/*
	 * delete the result in the database service
	 */
	public void delete() {
		philosophyRepo.deleteAll();
	}
}
