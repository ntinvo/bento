package com.example.demo.controller;

import java.util.ArrayList;
import java.util.HashSet;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.PhilosophyResult;
import com.example.demo.service.UrlService;

@RestController
public class UrlController {
	@Autowired
	private UrlService urlService;
	private static int DEFAULT_MAX_HOPS = 250;
	private static final String targetUrl = "https://en.wikipedia.org/wiki/Philosophy";
	
	/*
	 * A post mapping that will take a provided wiki url and find the paths to the wiki
	 * philosophy page and save those paths to the database.
	 * 
	 */
	@RequestMapping("/philosophy")
	@ResponseBody
	@CrossOrigin
	public PhilosophyResult test(@RequestParam String providedUrl) throws Exception {
		// create some temporary variable to handle the process
		StringBuilder currentUrl = new StringBuilder(providedUrl);
		HashSet<String> visitedUrls = new HashSet<>();
		ArrayList<String> results = new ArrayList<>();
		int count = 0;
		
		// we continue until we reach MAX HOPS or reach the target
		while(count < DEFAULT_MAX_HOPS && !currentUrl.toString().equals(targetUrl)) {
			
			// use Jsoup to parse the response and remove unnecessary elements
			Document doc;
			Elements urls;
			try {
				doc = Jsoup.connect(currentUrl.toString()).get();
			} catch (Exception e) {
				break;
			}
			urls = doc.select("body #mw-content-text");
			urls.select("div[role=note]").remove();
			urls.select("table").remove();
			urls = urls.select("p,li");
			urls = urls.select("a[href~=^((?![:.()#%]).)*$]");
			urls = urls.select("a[href~=^/wiki/]");
			
			// go through the links, if that link has not been visited, then we use
			// it and break out of the loop, else we just go to the next link
			for(int i = 0; i < urls.size(); i++) {
				Element urlElem = urls.get(i);
				String url = urlElem.absUrl("href");
				if(!visitedUrls.contains(url)) {
					currentUrl.setLength(0);
					currentUrl.append(url);
					visitedUrls.add(url);
					break;
				}
			}
			
			// add the processed link to the result for saving
			results.add(currentUrl.toString());
			count++;
		}
		
		// save the results to the database
		if(results.size() > 0) {
			PhilosophyResult res = urlService.create(results, providedUrl, count);
			return res;
		} else {
			PhilosophyResult res = new PhilosophyResult(new ArrayList<>(), providedUrl, -1);
			return res;
		}
	}
	
	
	/*
	 * A mapping for the index route
	 * 
	 */
	@RequestMapping("/")
	@ResponseBody
	@CrossOrigin
	public String index()  {
		return "Please go to https://bitbucket.org/tinnvo/bento/src/master/README.md for instructions";
	}
	
	/*
	 * A post mapping that will remove all of the records in the database
	 * 
	 */
	@RequestMapping("/delete")
	@ResponseBody
	@CrossOrigin
	public String delete()  {
		urlService.delete();
		return "Records deleted";
	}
}
