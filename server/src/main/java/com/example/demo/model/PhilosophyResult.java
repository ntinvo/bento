package com.example.demo.model;

import java.util.ArrayList;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class PhilosophyResult {
	@Id
	private String id;
	private int hops;
	private ArrayList<Url> urls;
	private String providedUrl;
	
	public PhilosophyResult(ArrayList<Url> urls, String providedUrl, int hops) {
		this.urls = urls;
		this.providedUrl = providedUrl;
		this.hops = hops;
	}

	public int getHops() {
		return hops;
	}

	public void setHops(int hops) {
		this.hops = hops;
	}

	public ArrayList<Url> getUrls() {
		return urls;
	}

	public void setUrls(ArrayList<Url> urls) {
		this.urls = urls;
	}

	public String getProvidedUrl() {
		return providedUrl;
	}

	public void setProvidedUrl(String providedUrl) {
		this.providedUrl = providedUrl;
	}
}
